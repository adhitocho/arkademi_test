// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:arkademi_test/controllers/course_controller.dart';
import 'package:arkademi_test/widgets/bab_tile.dart';
import 'package:arkademi_test/widgets/materi_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Kurikulum extends StatefulWidget {
  const Kurikulum({Key? key}) : super(key: key);

  @override
  State<Kurikulum> createState() => _KurikulumState();
}

class _KurikulumState extends State<Kurikulum> {
  CourseController? courseController;

  @override
  void initState() {
    super.initState();
    courseController = Provider.of<CourseController>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    final courseController = Provider.of<CourseController>(context);


    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: courseController.curriculum!.length,
      itemBuilder: ((BuildContext context, int index) {
        if (courseController.curriculum![index].id == 0) {
          return BabTile(
            judulBab: courseController.curriculum![index].title!,
            durasi:
                (courseController.curriculum![index].duration! / 60).round().toString(),
          );
        }
        return MateriTile(
          onTap: () {
            courseController.playVideo(index: index);
          },
          isDownloaded: () {},
          judulMateri: courseController.curriculum![index].title!,
          durasi: (courseController.curriculum![index].duration! / 60).round().toString(),
        );
      }),
    );
  }
}
