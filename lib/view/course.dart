// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, non_constant_identifier_names

import 'package:arkademi_test/controllers/course_controller.dart';
import 'package:arkademi_test/helpers/theme_library.dart';
import 'package:arkademi_test/view/materi/ikhtisar.dart';
import 'package:arkademi_test/view/materi/kurikulum.dart';
import 'package:arkademi_test/view/materi/lampiran.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:video_player/video_player.dart';

class Course extends StatefulWidget {
  const Course({Key? key}) : super(key: key);

  @override
  State<Course> createState() => _CourseState();
}

class _CourseState extends State<Course> with SingleTickerProviderStateMixin {
  CourseController? courseController;
  TabController? tabController;
  int selectedMateri = 0;

  @override
  void initState() {
    super.initState();
    courseController = Provider.of<CourseController>(context, listen: false);
    courseController?.getCourses().then(
          (value) => courseController?.playVideo(index: 1),
        );
    tabController = TabController(initialIndex: 0, length: 3, vsync: this);
  }

  @override
  void dispose() {
    courseController?.videoController.dispose();
    super.dispose();
  }

  List<Widget> TabBarPages = [
    Kurikulum(),
    Ikhtisar(),
    Lampiran(),
  ];

  @override
  Widget build(BuildContext context) {
    final courseController = Provider.of<CourseController>(context);
    final width = MediaQuery.of(context).size.width;

    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.arrow_back_rounded,
              color: Colors.black,
            ),
          ),
          titleSpacing: 0,
          title: Text(
            'Akuntansi Dasar dan Keuangan',
            style: ThemeLibrary.heading,
          ),
          backgroundColor: Colors.white,
        ),
        body: courseController.isLoading == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    AspectRatio(
                      aspectRatio: 16 / 9,
                      child: InkWell(
                        onTap: () => courseController.videoController.value.isPlaying
                            ? courseController.videoController.pause()
                            : courseController.videoController.play(),
                        child: Stack(
                          children: [
                            SizedBox(
                              width: width,
                              child: VideoPlayer(courseController.videoController),
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: VideoProgressIndicator(
                                courseController.videoController,
                                allowScrubbing: true,
                              ),
                            ),
                            courseController.videoController.value.isPlaying
                                ? SizedBox()
                                : Align(
                                    alignment: AlignmentDirectional.center,
                                    child: Container(
                                      padding: EdgeInsets.all(6),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white.withOpacity(0.3),
                                      ),
                                      child: Container(
                                        width: 45,
                                        height: 45,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white.withOpacity(0.8),
                                        ),
                                        child: Icon(
                                          Icons.play_arrow_sharp,
                                          color: Color(0xff0972c7),
                                          size: 35,
                                        ),
                                      ),
                                    ),
                                  )
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 15),
                      child: Align(
                        alignment: AlignmentDirectional.centerStart,
                        child: Text(
                          courseController.videoTitle,
                          textAlign: TextAlign.left,
                          style: ThemeLibrary.caption,
                        ),
                      ),
                    ),
                    StickyHeader(
                      header: Container(
                        color: Colors.white,
                        child: TabBar(
                          indicatorColor: Color(0xff0972c7),
                          onTap: (index) {
                            setState(() {
                              selectedMateri = index;
                            });
                          },
                          tabs: <Widget>[
                            Tab(
                              child: Text(
                                'Kurikulum',
                                textAlign: TextAlign.center,
                                style: ThemeLibrary.titleBold,
                              ),
                            ),
                            Tab(
                              child: Text(
                                'Ikhtisar',
                                textAlign: TextAlign.center,
                                style: ThemeLibrary.titleBold,
                              ),
                            ),
                            Tab(
                              child: Text(
                                'Lampiran',
                                textAlign: TextAlign.center,
                                style: ThemeLibrary.titleBold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      content: courseController.courses == null
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : TabBarPages[selectedMateri],
                    ),
                  ],
                ),
              ),
        bottomNavigationBar: Container(
          height: 50,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 5,
              offset: Offset(0, -2),
            )
          ]),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.keyboard_double_arrow_left_sharp,
                        color: Colors.black,
                      ),
                      Text(
                        'Sebelumnya',
                        textAlign: TextAlign.center,
                        style: ThemeLibrary.titleBold,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Selanjutnya',
                        textAlign: TextAlign.center,
                        style: ThemeLibrary.titleBoldGrey,
                      ),
                      Icon(
                        Icons.keyboard_double_arrow_right_sharp,
                        color: ThemeLibrary.grey,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
