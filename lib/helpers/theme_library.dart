import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeLibrary {
  static const grey = Color(0xff797a7b);
  static const grey2 = Color(0xfff1f2f4);

  static final heading = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  static final titleBold = GoogleFonts.roboto(
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  static final titleBoldGrey = GoogleFonts.roboto(
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: grey,
  );

  static final title = GoogleFonts.roboto(
    fontSize: 12,
    fontWeight: FontWeight.normal,
    color: Colors.black,
  );

  static final caption = GoogleFonts.roboto(
    fontSize: 10,
    fontWeight: FontWeight.normal,
    color: grey,
  );

  static final tileVideoButton = GoogleFonts.roboto(
    fontSize: 8,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  static final tileVideoButtonWhite = GoogleFonts.roboto(
    fontSize: 8,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
}