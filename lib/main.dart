// ignore_for_file: prefer_const_constructors

import 'package:arkademi_test/controllers/course_controller.dart';
import 'package:arkademi_test/view/course.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CourseController>(
          create: (_) => CourseController(),
        ),
      ],
      child: MaterialApp(
        title: 'Arkademi Test - Eka Adhitya K.',
        theme: ThemeData(
          primaryColor: const Color(0xff22bb3e),
          primarySwatch: Colors.blue,
        ),
        home: Course(),
      ),
    );
  }
}
