import 'dart:convert';

import 'package:arkademi_test/models/courses.dart';
import 'package:arkademi_test/models/curriculum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:video_player/video_player.dart';

class CourseController extends ChangeNotifier {
  CoursesResp? courses;
  List<CurriculumResp>? curriculum;
  late VideoPlayerController videoController;
  int selectedCourse = 1;
  String videoTitle = '';
  bool isLoading = true;
  Stream<FileResponse>? fileStream;

  Future<CoursesResp> coursesResp() async {
    final response = await rootBundle.loadString('json/courses.json');
    final decode = jsonDecode(response);
    return CoursesResp.fromJson(decode);
  }

  Future<void> getCourses() async {
    final response = await coursesResp();
    courses = response;
    curriculum = response.curriculum;
  }

  void playVideo({required int index}) {
    videoController = VideoPlayerController.network(
        curriculum?[selectedCourse].online_video_link ?? '');
    videoController.addListener(() {
      notifyListeners();
    });
    videoController.initialize().then((_) {
      notifyListeners();
    }).then(
          (value) {
        videoController.play();
        notifyListeners();
      },
    );
    isLoading = false;
    selectedCourse = index;
    videoTitle = curriculum?[selectedCourse].title!.replaceAll('&#8211;', '-') ?? '';
  }
}
