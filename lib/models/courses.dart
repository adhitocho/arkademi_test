// ignore_for_file: non_constant_identifier_names, depend_on_referenced_packages

import 'package:json_annotation/json_annotation.dart';
import 'curriculum.dart';

part 'courses.g.dart';

@JsonSerializable(createToJson: false)
class CoursesResp {
  String? course_name;
  String? progress;
  List<CurriculumResp>? curriculum;

  CoursesResp(
    this.course_name,
    this.progress,
    this.curriculum,
  );

  factory CoursesResp.fromJson(Map<String, dynamic> json) => _$CoursesRespFromJson(json);
}
