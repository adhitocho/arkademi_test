// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'courses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoursesResp _$CoursesRespFromJson(Map<String, dynamic> json) => CoursesResp(
      json['course_name'] as String?,
      json['progress'] as String?,
      (json['curriculum'] as List<dynamic>?)
          ?.map((e) => CurriculumResp.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
