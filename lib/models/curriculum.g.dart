// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'curriculum.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurriculumResp _$CurriculumRespFromJson(Map<String, dynamic> json) =>
    CurriculumResp(
      json['key'] as int?,
      json['id'],
      json['type'] as String?,
      json['title'] as String?,
      json['duration'] as int?,
      json['content'] as String?,
      json['status'] as int?,
      json['meta'] as List<dynamic>?,
      json['online_video_link'] as String?,
      json['offline_video_link'] as String?,
    );
