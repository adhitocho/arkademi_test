// ignore_for_file: non_constant_identifier_names, depend_on_referenced_packages

import 'package:json_annotation/json_annotation.dart';

part 'curriculum.g.dart';

@JsonSerializable(createToJson: false)
class CurriculumResp {
  int? key;
  dynamic id;
  String? type;
  String? title;
  int? duration;
  String? content;
  int? status;
  List? meta;
  String? online_video_link;
  String? offline_video_link;

  CurriculumResp(
    this.key,
    this.id,
    this.type,
    this.title,
    this.duration,
    this.content,
    this.status,
    this.meta,
    this.online_video_link,
    this.offline_video_link,
  );

  factory CurriculumResp.fromJson(Map<String, dynamic> json) => _$CurriculumRespFromJson(json);
}
