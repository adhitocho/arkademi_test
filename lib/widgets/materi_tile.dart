// ignore_for_file: prefer_const_constructors

import 'package:arkademi_test/controllers/course_controller.dart';
import 'package:arkademi_test/helpers/theme_library.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MateriTile extends StatefulWidget {
  final String judulMateri;
  final String durasi;
  final VoidCallback onTap;
  final VoidCallback isDownloaded;

  const MateriTile({
    Key? key,
    required this.judulMateri,
    required this.durasi,
    required this.onTap,
    required this.isDownloaded,
  }) : super(key: key);

  @override
  State<MateriTile> createState() => _MateriTileState();
}

class _MateriTileState extends State<MateriTile> {
  CourseController? courseController;

  @override
  void initState() {
    super.initState();
    courseController = Provider.of<CourseController>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return InkWell(
      onTap: widget.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        width: width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Align(
              alignment: AlignmentDirectional.topCenter,
              child: Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xff9da0a7),
                ),
                child: Icon(
                  Icons.check,
                  size: 10,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.judulMateri.replaceAll('&#8211;', '-'),
                    textAlign: TextAlign.left,
                    maxLines: 2,
                    style: ThemeLibrary.title,
                  ),
                  SizedBox(height: 3),
                  Text(
                    '${widget.durasi} Menit',
                    textAlign: TextAlign.left,
                    style: ThemeLibrary.caption,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: widget.isDownloaded,
                child: Container(
                  height: 20,
                  decoration: BoxDecoration(
                    color: Color(0xff0972c7),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Tonton Offline',
                        textAlign: TextAlign.center,
                        style: ThemeLibrary.tileVideoButtonWhite,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // Expanded(
            //   flex: 1,
            //   child: Container(
            //     height: 20,
            //     decoration: BoxDecoration(
            //       border: Border.all(
            //         color: Color(0xff9da0a7),
            //         width: 0.2,
            //       ),
            //     ),
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.center,
            //       crossAxisAlignment: CrossAxisAlignment.center,
            //       children: [
            //         Text(
            //           'Tersimpan',
            //           textAlign: TextAlign.center,
            //           style: ThemeLibrary.tileVideoButton,
            //         ),
            //         SizedBox(width: 3,),
            //         Icon(
            //           Icons.check_circle_outline,
            //           color: Color(0xff0972c7),
            //           size: 10,
            //         )
            //       ],
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}