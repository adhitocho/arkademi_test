import 'package:arkademi_test/helpers/theme_library.dart';
import 'package:flutter/material.dart';

class BabTile extends StatelessWidget {
  final String judulBab;
  final String durasi;

  const BabTile({
    Key? key,
    required this.judulBab,
    required this.durasi,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: ThemeLibrary.grey2,
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              judulBab,
              textAlign: TextAlign.left,
              style: ThemeLibrary.titleBold,
            ),
            const SizedBox(height: 3),
            Text(
              '$durasi Menit',
              textAlign: TextAlign.left,
              style: ThemeLibrary.caption,
            ),
          ],
        ),
      ),
    );
  }
}
